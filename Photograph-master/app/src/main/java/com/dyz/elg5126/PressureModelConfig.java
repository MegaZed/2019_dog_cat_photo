package com.dyz.elg5126;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PressureModelConfig {
    public static String MODEL_FILENAME = "converted_model.tflite";
    //public static final int INPUT_TrainingData_SIZE = 100;
    public static final int INPUT_IMG_SIZE_WIDTH = 128;
    public static final int INPUT_IMG_SIZE_HEIGHT = 128;
    //r,g,b 1 per
    public static final int DIM_PIXEL_SIZE = 3;
    //one picture per test
    public static final int DIM_BATCH_SIZE = 1;

    public static final int FLOAT_TYPE_SIZE = 4;
    public static final int Single_SIZE = 1;
    public static final int MODEL_INPUT_SIZE = DIM_PIXEL_SIZE * DIM_BATCH_SIZE
            * FLOAT_TYPE_SIZE * INPUT_IMG_SIZE_HEIGHT * INPUT_IMG_SIZE_WIDTH;
    public static final List<String> OUTPUT_LABELS = Collections.unmodifiableList(
            Arrays.asList("cat", "dog"));
    public static final int MAX_CLASSIFICATION_RESULTS = 3;
    public static final float CLASSIFICATION_THRESHOLD = 0.5f;
}

