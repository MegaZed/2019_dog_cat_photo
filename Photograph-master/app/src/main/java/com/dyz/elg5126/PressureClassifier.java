package com.dyz.elg5126;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;

import org.tensorflow.lite.Interpreter;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import static com.dyz.elg5126.PressureModelConfig.CLASSIFICATION_THRESHOLD;
import static com.dyz.elg5126.PressureModelConfig.MAX_CLASSIFICATION_RESULTS;

//This class is used to deal with pretrained model(including loadModel, set model's input and output)
public class PressureClassifier {
    //The TensorFlow Lite interpreter is a library that takes a model file, executes the operations it defines on input data, and provides access to the output.
    private final Interpreter interpreter;
    private static final int IMAGE_MEAN = 0;
    private static final float IMAGE_STD = 255.0f;

    private PressureClassifier(Interpreter interpreter) {
        this.interpreter = interpreter;
    }

    //return a new PressureClassifier object based on the modelPath
    public static PressureClassifier classifier(AssetManager assetManager, String modelPath) throws IOException {
        ByteBuffer byteBuffer = loadModelFile(assetManager, modelPath);
        Interpreter interpreter = new Interpreter(byteBuffer);
        return new PressureClassifier(interpreter);
    }

    //private method to convert .tflite model file to ByteBuffer
    private static ByteBuffer loadModelFile(AssetManager assetManager, String modelPath) throws IOException {
        AssetFileDescriptor fileDescriptor = assetManager.openFd(modelPath);
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }

    public List<Classification> recognizeImage(Bitmap bitmap) {
        ByteBuffer byteBuffer = convertTestDataToByteBuffer(bitmap);
        float[][] result = new float[1][PressureModelConfig.OUTPUT_LABELS.size()];
        interpreter.run(byteBuffer, result);
        return getSortedResult(result);
    }
    //public method to get output of the model, define the input PressureList as a float array

    //private method used to convert TestData to ByteBuffer to be the direct input of interpreter
    private ByteBuffer convertTestDataToByteBuffer(Bitmap bitmap) {
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(PressureModelConfig.MODEL_INPUT_SIZE);
        byteBuffer.order(ByteOrder.nativeOrder());
        int[] pixels = new int[PressureModelConfig.INPUT_IMG_SIZE_WIDTH * PressureModelConfig.INPUT_IMG_SIZE_HEIGHT];
        bitmap.getPixels(pixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        int pixel = 0;
        for (int i = 0; i < PressureModelConfig.INPUT_IMG_SIZE_WIDTH; ++i) {
            for (int j = 0; j < PressureModelConfig.INPUT_IMG_SIZE_HEIGHT; ++j) {
                final int val = pixels[pixel++];
                byteBuffer.putFloat((((val >> 16) & 0xFF)-IMAGE_MEAN)/IMAGE_STD);
                byteBuffer.putFloat((((val >> 8) & 0xFF)-IMAGE_MEAN)/IMAGE_STD);
                byteBuffer.putFloat(((val & 0xFF)-IMAGE_MEAN)/IMAGE_STD);
            }
        }
        System.out.println("kkkkkkkkkkk");
        return byteBuffer;
    }

    //private method to get label from max confidence to min confidence
    private List<Classification> getSortedResult(float[][] resultsArray) {
        PriorityQueue<Classification> sortedResults = new PriorityQueue<>(
                MAX_CLASSIFICATION_RESULTS,
                (lhs, rhs) -> Float.compare(rhs.confidence, lhs.confidence)
        );

        for (int i = 0; i < PressureModelConfig.OUTPUT_LABELS.size(); ++i) {
            float confidence = resultsArray[0][i];
            if (confidence > CLASSIFICATION_THRESHOLD) {
                PressureModelConfig.OUTPUT_LABELS.size();
                sortedResults.add(new Classification(PressureModelConfig.OUTPUT_LABELS.get(i), confidence));
            }
        }
        return new ArrayList<>(sortedResults);
    }
}


