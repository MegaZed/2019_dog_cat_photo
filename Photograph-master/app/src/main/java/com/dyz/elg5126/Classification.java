package com.dyz.elg5126;

//define a predicting result class containing both label and label's confidence interval
public class Classification {
    public final String title;
    public final float confidence;

    public Classification(String title, float confidence) {
        this.title = title;
        this.confidence = confidence;
    }


    public String toString1() {
        return title;
    }
    public String toString2() {
        return String.format("(%.1f%%)   ", confidence * 100.0f);
    }


}
