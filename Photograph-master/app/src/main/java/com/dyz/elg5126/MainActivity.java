package com.dyz.elg5126;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.os.EnvironmentCompat;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button inferButton, Button3;
    TextView outputNumber3, outputNumber4;
    Spinner spinner1;
    int Itemposition = 0;
    private PressureClassifier pressureClassifier;

    private ImageView ivCamera;
    private ImageView ivPhoto;

    // 拍照的requestCode
    private static final int CAMERA_REQUEST_CODE = 0x00000010;
    // 申请相机权限的requestCode
    private static final int PERMISSION_CAMERA_REQUEST_CODE = 0x00000012;
    /**
     * 用于保存拍照图片的uri
     */
    private Uri mCameraUri;

    /**
     * 用于保存图片的文件路径，Android 10以下使用图片路径访问图片
     */
    private String mCameraImagePath;

    /**
     * 是否是Android 10以上手机
     */
    private boolean isAndroidQ = Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadPressureClassifier();
        System.out.println("doable lol");
        initView();
    }


    private void initView(){
        outputNumber3 = (TextView)findViewById(R.id.s7_show);
        outputNumber4 = (TextView)findViewById(R.id.s8_show);
        inferButton = (Button)findViewById(R.id.button1);
        //Button3 = (Button) findViewById(R.id.button3);
        //imageview2 = (ImageView) findViewById(R.id.imageView2);
        spinner1 = findViewById(R.id.spinner);
        ivCamera = findViewById(R.id.ivCamera);
        ivPhoto = findViewById(R.id.imageView2);

        //spinner1 in layout is the function of choosing file
        //spinner1 configuration is in res\\values\\strings.xml
        //The next several rows are all about using spinner1
        String[] mItems = getResources().getStringArray(R.array.TestFiles);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mItems);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter);
        spinner1.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            //when choosing an item in spinner list
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id){
                //give the current choice position number to Itemposition
                Itemposition = pos;
            }

            public void onNothingSelected(AdapterView<?> parent){
            }
        });

        inferButton.setOnClickListener(this);
        ivCamera.setOnClickListener(this);
        System.out.println("wjadkaskdjkl");
    }

    @Override
    public void onClick(View view){
        switch (view.getId()) {
            case R.id.ivCamera:
                checkPermissionAndCamera();
                break;
            case R.id.button1:
                get_demo_pre();
                break;
            default:
                break;
        }
    }

    /**
     * 检查权限并拍照。
     * 调用相机前先检查权限。
     */
    private void checkPermissionAndCamera(){
        int hasCameraPermission = ContextCompat.checkSelfPermission(getApplication(),
                Manifest.permission.CAMERA);
        if (hasCameraPermission == PackageManager.PERMISSION_GRANTED) {
            //有权限，调起相机拍照。
            openCamera();
        } else {
            //没有权限，申请权限。
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    PERMISSION_CAMERA_REQUEST_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (isAndroidQ) {
                    // Android 10 使用图片uri加载
                    ivPhoto.setImageURI(mCameraUri);
                } else {
                    // 使用图片路径加载
                    Bitmap bitmapk = BitmapFactory.decodeFile(mCameraImagePath);
                    ivPhoto.setImageBitmap(bitmapk);
                    Bitmap resizedBitmap = Bitmap.createScaledBitmap(
                            bitmapk, PressureModelConfig.INPUT_IMG_SIZE_WIDTH, PressureModelConfig.INPUT_IMG_SIZE_HEIGHT, false);
                    List<Classification> recognitions1 = pressureClassifier.recognizeImage(resizedBitmap);
                    //if (recognitions1.get(0)==null)
                    outputNumber3.setText(recognitions1.get(0).toString1());
                    outputNumber4.setText(recognitions1.get(0).toString2());
                }
            } else {
                Toast.makeText(this, "取消", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * 处理权限申请的回调。
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        if (requestCode == PERMISSION_CAMERA_REQUEST_CODE) {
            if (grantResults.length > 0
                    && grantResults[ 0 ] == PackageManager.PERMISSION_GRANTED) {
                //允许权限，有调起相机拍照。
                openCamera();
            } else {
                //拒绝权限，弹出提示框。
                Toast.makeText(this, "拍照权限被拒绝", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * 调起相机拍照
     */
    private void openCamera(){
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // 判断是否有相机
        if (captureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            Uri photoUri = null;

            if (isAndroidQ) {
                // 适配android 10
                photoUri = createImageUri();
            } else {
                try {
                    photoFile = createImageFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (photoFile != null) {
                    mCameraImagePath = photoFile.getAbsolutePath();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        //适配Android 7.0文件权限，通过FileProvider创建一个content类型的Uri
                        photoUri = FileProvider.getUriForFile(this, getPackageName() + ".fileprovider", photoFile);
                    } else {
                        photoUri = Uri.fromFile(photoFile);
                    }
                }
            }

            mCameraUri = photoUri;
            if (photoUri != null) {
                captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                captureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                startActivityForResult(captureIntent, CAMERA_REQUEST_CODE);
            }
        }
    }

    /**
     * 创建图片地址uri,用于保存拍照后的照片 Android 10以后使用这种方法
     *
     * @return 图片的uri
     */
    private Uri createImageUri(){
        String status = Environment.getExternalStorageState();
        // 判断是否有SD卡,优先使用SD卡存储,当没有SD卡时使用手机存储
        if (status.equals(Environment.MEDIA_MOUNTED)) {
            return getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new ContentValues());
        } else {
            return getContentResolver().insert(MediaStore.Images.Media.INTERNAL_CONTENT_URI, new ContentValues());
        }
    }

    /**
     * 创建保存图片的文件
     *
     * @return
     * @throws IOException
     */
    private File createImageFile() throws IOException{
        String imageName = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (!storageDir.exists()) {
            storageDir.mkdir();
        }
        File tempFile = new File(storageDir, imageName);
        if (!Environment.MEDIA_MOUNTED.equals(EnvironmentCompat.getStorageState(tempFile))) {
            return null;
        }
        return tempFile;
    }


    private void loadPressureClassifier(){
        try {
            pressureClassifier = PressureClassifier.classifier(getAssets(), PressureModelConfig.MODEL_FILENAME);
        } catch (IOException e) {
            Toast.makeText(this, "Pressure model couldn't be loaded. Check logs for details.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public void get_demo_pre(){
        InputStream mAssets = null;
        try {
            String[] filenames = getBaseContext().getAssets().list("");
            mAssets = getAssets().open(filenames[Itemposition]);
            Bitmap bitmap1 = BitmapFactory.decodeStream(mAssets);
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(
                    bitmap1, PressureModelConfig.INPUT_IMG_SIZE_WIDTH, PressureModelConfig.INPUT_IMG_SIZE_HEIGHT, false);
            BitmapDrawable bmpDraw;
            Bitmap view1;
            view1 = bitmap1;
            bmpDraw = new BitmapDrawable(view1);
            ivPhoto.setImageDrawable(bmpDraw);
            List<Classification> recognitions1 = pressureClassifier.recognizeImage(resizedBitmap);
            //if (recognitions1.get(0)==null)
            outputNumber3.setText(recognitions1.get(0).toString1());
            outputNumber4.setText(recognitions1.get(0).toString2());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

